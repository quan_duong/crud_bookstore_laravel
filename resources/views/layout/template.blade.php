
/**
 * Created by PhpStorm.
 * User: QuanDT
 * Date: 11/24/15
 * Time: 00:25
 */
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>BookStore</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap
/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
@yield('content')
        </div>
</body>
</html>